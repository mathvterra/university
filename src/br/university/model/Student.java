package br.university.model;

public class Student extends Person{
	public Student(String name, String address, Course course) {
		this.name = name;
		this.address = address;
		this.course = course;
	}	
	
	private Course course;

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
}
