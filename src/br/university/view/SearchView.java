package br.university.view;

import java.util.List;

import br.university.controller.*;

public class SearchView {
	public SearchView(UniversityController controller) {
		this.university = controller;
	}

	UniversityController university;

	public void onScreen() {
		System.out.println("Digite 1 para mostrar lista dos cursos dispon�veis; ");
		System.out.println("Digite 2 para listar os alunos por curso; ");
		System.out.println("Digite 3 para mostrar o professor de um curso; ");
		System.out.println("Digite exit para sair ou back pra voltar. ");

		String input = MainView.scanner.nextLine().toLowerCase();

		switch (input) {
		case "1":
			showCourseList();
			break;
		case "2":
			showStudentsByCourse();
			break;
		case "3":
			showTeacherByCourse();
			break;
		case "back":
			break;
		case "exit":
			System.exit(0);
		}
	}

	private void showCourseList() {
		List<String> courses = this.university.getCourseList();
		if (courses.isEmpty())
			System.out.print("N�o existem cursos cadastrados!");
		else
			courses.forEach(c -> System.out.println(c));
	}

	private void showStudentsByCourse() {
		System.out.println("Digite o nome do curso");
		String courseName = MainView.scanner.nextLine().toLowerCase();

		List<String> students = this.university.getStudentsByCourse(courseName);
		if (students.isEmpty())
			System.out.print("O curso n�o foi encontrado ou n�o tem estudantes cadastrados!");
		else
			students.forEach(s -> System.out.println(s));
	}

	private void showTeacherByCourse() {
		System.out.println("Digite o nome do curso");
		String courseName = MainView.scanner.nextLine().toLowerCase();

		System.out.println(this.university.getTeacherByCourse(courseName));
	}
}