package br.university.view;

import br.university.controller.UniversityController;

public class EnrollmentView {
	public EnrollmentView(UniversityController controller) {
		this.university = controller;
	}

	UniversityController university;

	public void onScreen() {
		System.out.println("Digite 1 para cadastrar novo professor; ");
		System.out.println("Digite 2 para cadastrar novo curso (necessario ja ter cadastrado um professor); ");
		System.out.println("Digite 3 para cadastrar novo aluno (necessario ja ter cadastrado um curso); ");
		System.out.println("Digite 4 para desmatricular um aluno (necessario ja ter cadastrado um curso e um aluno); ");
		System.out.println("Digite exit para sair ou back pra voltar. ");

		String input = MainView.scanner.nextLine().toLowerCase();

		switch (input) {
		case "1":
			registerTeacher();
			break;
		case "2":
			registerCourse();
			break;
		case "3":
			registerStudent();
			break;
		case "4":
			unregisterStudent();
			break;
		case "back":
			break;
		case "exit":
			System.exit(0);
		}
	}

	private void registerCourse() {
		System.out.println("Digite o nome do professor");
		String teacherName = MainView.scanner.nextLine().toLowerCase();

		System.out.println("Digite o nome do curso");
		String courseName = MainView.scanner.nextLine().toLowerCase();
		if (university.addToCourseList(teacherName, courseName))
			System.out.println("Curso adicionado com sucesso");
		else
			System.out.println("Houve um erro ao adicionar um curso, tente novamente!");
	}

	private void registerStudent() {
		System.out.println("Digite o nome do curso para registrar um aluno");
		String courseName = MainView.scanner.nextLine().toLowerCase();
		System.out.println("Digite o nome do aluno");
		String studentName = MainView.scanner.nextLine().toLowerCase();
		System.out.println("Digite o endereco do aluno");
		String studentAddress = MainView.scanner.nextLine().toLowerCase();

		if (university.addToStudentList(studentName, studentAddress, courseName))
			System.out.println("Estudante adicionado com sucesso!");
		else
			System.out.print("Houve um erro ao adicionar estudante, tente novamente!");
	}

	private void registerTeacher() {
		System.out.println("Digite o nome do professor");
		String teacherName = MainView.scanner.nextLine().toLowerCase();
		System.out.println("Digite o endereco do professor");
		String teacherAddress = MainView.scanner.nextLine().toLowerCase();
		System.out.println("Digite o departamento do professor");
		String teacherDepartment = MainView.scanner.nextLine().toLowerCase();

		if (this.university.addToTeachersList(teacherName, teacherAddress, teacherDepartment))
			System.out.print("Professor adicionado com sucesso!");
	}
	
	private void unregisterStudent() {
		System.out.println("Digite o nome do aluno");
		String studentName = MainView.scanner.nextLine().toLowerCase();
		System.out.println("Digite o nome do curso");
		String courseName = MainView.scanner.nextLine().toLowerCase();
		
		if (this.university.removeStudent(studentName, courseName))
			System.out.print("Aluno desmatriculado com sucesso!");
	}
}
