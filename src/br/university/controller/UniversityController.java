package br.university.controller;

import java.util.ArrayList;
import java.util.List;

import br.university.model.*;

public class UniversityController {
	public final static String COURSESFILEPATH = "courses.dat";
	
	public UniversityController() {
		this.loadCourses(COURSESFILEPATH);
	}
	
	private List<Teacher> teachersList = new ArrayList<Teacher>();
	private List<Course> coursesList = new ArrayList<Course>();	
	private CourseSerializer serializer = new CourseSerializer();
	
	public String getTeacherByCourse(String courseName) {
		Course course = this.coursesList.stream().filter(c -> c.getName().equals(courseName)).findFirst().orElse(null);
		
		if (course != null)	
			return course.getTeacher().getName();
		else
			return "Curso nao encontrado";
	}
	
	public boolean addToTeachersList(String teacherName, String teacherAddres, String teacherDepartment) {
		this.teachersList.add(new Teacher(teacherName, teacherAddres, teacherDepartment));
		return true;
	}
	
	public List<String> getCourseList() {
		List<String> courseNames = new ArrayList<String>();
		this.coursesList.forEach(c -> courseNames.add(c.getName()));
		return courseNames;
	}
	
	public boolean addToCourseList(String teacherName, String courseName) {
		Teacher teacher = this.teachersList.stream().filter(t -> t.getName().equals(teacherName)).findFirst().orElse(null);
		boolean courseExists = this.coursesList.stream().anyMatch(c -> c.getName().equals(courseName));
		
		if (teacher != null && !courseExists)
		{			
			Course course = new Course(coursesList.size(), courseName, teacher);  
			this.coursesList.add(course);
			this.serializer.serialize(this.coursesList, COURSESFILEPATH);
			return true;
		}		
		return false;
	}
	
	public List<String> getStudentsByCourse(String courseName) {
		Course course = this.coursesList.stream().filter(c -> c.getName().equals(courseName)).findFirst().orElse(null);
		
		List<String> students = new ArrayList<String>();
		course.getStudentsEnrolled().forEach(s -> students.add(s.getName()));
		return students;		
	}
	
	public boolean addToStudentList(String studentName, String studenAddress, String courseName) {
		Course course = this.coursesList.stream().filter(c -> c.getName().equals(courseName)).findFirst().orElse(null);
		
		if (course != null) {
			Student student = new Student(studentName, studenAddress, course);
			course.addStudentEnrolled(student);
			this.serializer.serialize(this.coursesList, COURSESFILEPATH);
			return true;
		}
		return false;
	}	
	
	public boolean removeStudent(String studentName, String courseName)
	{
		Course course = this.coursesList.stream().filter(c -> c.getName().equals(courseName)).findFirst().orElse(null);
		if (course != null) {
			course.getStudentsEnrolled().removeIf(s->s.getName() == studentName);
			this.serializer.serialize(this.coursesList, COURSESFILEPATH);
			return true;
		}
		return false;
	}
	
	public void saveCoursesInfo(String filePath) {
		this.serializer.serialize(this.coursesList, filePath);
	}
	
	public void loadCourses(String filePath){
		this.coursesList.addAll(this.serializer.deserialize(filePath));
		this.loadTeachers();
	}
	
	private void loadTeachers() {
		this.coursesList.forEach(c -> this.teachersList.add(c.getTeacher()));
	}
}
