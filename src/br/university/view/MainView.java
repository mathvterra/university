package br.university.view;

import java.util.Scanner;

import br.university.controller.*;

public class MainView {
	public MainView(UniversityController controller) {
		this.university = controller;
	}

	public static Scanner scanner = new Scanner(System.in);
	public UniversityController university;

	public void onScreen() {
		while (true) {
			System.out.println("Digite 1 para pesquisar informações; ");
			System.out.println("Digite 2 para cadastrar informações; ");
			System.out.println("Digite exit para sair. ");

			String input = MainView.scanner.nextLine().toLowerCase();

			switch (input) {
			case "1":
				SearchView searchView = new SearchView(this.university);
				searchView.onScreen();
				break;
			case "2":
				EnrollmentView enrollmentView = new EnrollmentView(this.university);
				enrollmentView.onScreen();
				break;
			case "exit":
				System.exit(0);
			}

			System.out.println();
			System.out.println();
		}
	}
}
