package br.university.model;

public class Teacher extends Person{		
	public Teacher(String name, String address, String department)
	{
		this.address = address;
		this.name = name;
		this.department = department;
	}
	
	String department;

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
}
