package br.university.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Course implements Serializable {	
	public Course() {};
	
	public Course(int code, String name, Teacher teacher)
	{
		this.code = code;
		this.name = name;
		this.teacher = teacher;
	}
	
	public Course(int code, String name, Teacher teacher, List<Student> students)
	{
		this.code = code;
		this.name = name;
		this.teacher = teacher;
		this.studentsEnrolled = students;
	}

	private int code;
	private String name;
	private Teacher teacher;	
	private Date schedule;
	private String room;
	private List<Student> studentsEnrolled = new ArrayList<Student>();
		
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Teacher getTeacher() {
		return teacher;
	}
	
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	
	public Date getSchedule() {
		return schedule;
	}
	
	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}
	
	public String getRoom() {
		return room;
	}
	
	public void setRoom(String room) {
		this.room = room;
	}
	
	public List<Student> getStudentsEnrolled()
	{
		return studentsEnrolled;
	}
	
	public void addStudentEnrolled(Student student)
	{
		this.studentsEnrolled.add(student);
	}
}
