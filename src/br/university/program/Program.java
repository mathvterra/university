package br.university.program;

import br.university.controller.*;
import br.university.view.*;

public class Program {
	public static void main(String[] args) {
		UniversityController university = new UniversityController();
		MainView mainView = new MainView(university);
		mainView.onScreen();
	}
}
